﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot
{
    class Program
    {
        static void Main()
        {
            MainAsync().Wait();
        }

        static Task MainAsync()
        {

            BotStarter starter = new BotStarter();
            return starter.Run();
        }
    }
}
